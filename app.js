const express = require("express");
const server = express();

const dict = {};

server.get("/api/timestamp/:time", (req, res) => {
    if (req.url.startsWith("/api/timestamp/")) {
        const dateString = req.params.time
        let timestamp = dateString;
        if (dateString === undefined || dateString.trim() === "") {
            timestamp = new Date().getTime();
        }

        dict.requestTimestamp = timestamp,
        dict.responseTimeStamp = new Date().getTime()

        console.log(dict)

        res.json(dict);

    }
});

server.listen(3000, () => {
    console.log("App listening at port 3000")

});


